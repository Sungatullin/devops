# Задание 4

## Описание 

Напишите docker compose конфиг, для разворачивания двух контейнеров
в одной сети (10.10.10.0/28) типа bridge: 
1 - Nginx,
ему должны передаваться конфигурационные файлы через volume, 
порт 80 из контейнера должен быть доступен на хостовой машине 
по порту 8080
2 - postgres, каталог для хранения данных 
должен монтироваться как docker volume, docker volume 
должен быть описан в том же конфигурационном файле docker compose. 
Сервис с БД должен быть доступен из контейнера с веб-сервером
по именам new_db, dev_db.

## Запущенные контейнеры

![](img.png)


