#!/bin/bash

# Инициализация базы данных и запуск PostgreSQL
service postgresql start

# Установим пароль для пользователя postgres
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD '$POSTGRES_PASSWORD';"

# Держим контейнер запущенным
tail -f /dev/null
