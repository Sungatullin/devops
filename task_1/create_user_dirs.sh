#!/bin/bash

log_message() {
    local message="$1"
    echo "$message" | tee -a create_user_dirs.log
}

get_root_directory() {
    read -rp "Please enter the root directory for creating user directories: " root_dir
}

while getopts "d:" opt; do
    case $opt in
        d)
            root_dir="$OPTARG"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

if [ -z "$root_dir" ]; then
    get_root_directory
fi

if [ ! -d "$root_dir" ]; then
    log_message "Error: The specified root directory does not exist."
    exit 1
fi

log_message "Starting to create user directories in: $root_dir"

while IFS=: read -r username _ _ _ _ _ home_directory _; do
    if [[ -z "$home_directory" || "$home_directory" == "/" || "$home_directory" == "/nonexistent" ]]; then
        continue
    fi

    user_dir="$root_dir/$username"
    if mkdir -p "$user_dir"; then
        log_message "Created directory: $user_dir"
        if chown "$username:$username" "$user_dir" && chmod 755 "$user_dir"; then
            log_message "Set ownership and permissions for: $user_dir"
        else
            log_message "Failed to set ownership or permissions for: $user_dir"
        fi
    else
        log_message "Failed to create directory: $user_dir"
    fi
done < /etc/passwd

log_message "Completed creating user directories."

