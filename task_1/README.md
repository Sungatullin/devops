# Задание 1

## Описание задачи
Написать bash скрипт, который создаст для всех пользователей 
системы отдельную директорию в корневой директории с именем пользователя
и установит для нее права 755. При этом владельцем директории должен быть 
соответствующий пользователь. Путь до корневого каталога создания
директорий, должен определяться через ключ "-d" или если ключ не задан
то должен быть "диалог" для определения пути пользователем. 
Лог должен писаться и в stdout и в файл.

## Пример запуска
```
./create_user_dirs.sh -d /home/user
```

## Пример лога
```
Starting to create user directories in: /home/user
Created directory: /home/user/sbr
Set ownership and permissions for: /home/user/sbr
Created directory: /home/user/user
Set ownership and permissions for: /home/user/user
Completed creating user directories.
```
